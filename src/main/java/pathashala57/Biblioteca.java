package pathashala57;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//represents virtual library
public class Biblioteca implements Library {
    private static final String NEW_LINE = System.lineSeparator();
    private static final String STRING_FORMAT_FOR_BOOK_INFORMATION = "%-35s %-35s %-35s" + NEW_LINE;
    private static final String STRING_FORMAT_FOR_MOVIE_INFORMATION = "%-35s %-35s %-35s %-35s" + NEW_LINE;

    private final List<LibraryItem> uncheckedOutLibraryItem;
    private final List<LibraryItem> checkedOutLibraryItem;
    private String booksInJSON;

    public Biblioteca(List<LibraryItem> itemList) {
        this.checkedOutLibraryItem = new ArrayList<>();
        if (itemList != null) {
            this.uncheckedOutLibraryItem = new ArrayList<>(itemList);
            return;
        }
        this.uncheckedOutLibraryItem = new ArrayList<>();
    }

    public String toStringRepresentationOfBooks() {
        List<LibraryItem> bookList = getItemList(Book.class);
        return headerForDisplayingBookList() + generateStringRepresentationOf(bookList);
    }

    public String toStringRepresentationOfMovies() {
        List<LibraryItem> movieList = getItemList(Movie.class);
        return headerForDisplayingMovieList() + generateStringRepresentationOf(movieList);
    }

    public List<LibraryItem> getItemList(Class classType) {
        return uncheckedOutLibraryItem
                .stream()
                .filter(x -> x.getClass() == classType)
                .collect(Collectors.toList());
    }

    public Optional<LibraryItem> checkoutItem(String itemName, Class itemType) {
        Optional<LibraryItem> libraryItem = getLibraryItem(itemName, itemType, uncheckedOutLibraryItem);
        libraryItem.ifPresent(libraryItem1 -> {
            uncheckedOutLibraryItem.remove(libraryItem1);
            checkedOutLibraryItem.add(libraryItem1);
        });
        return libraryItem;
    }

    public boolean returnItem(String inputItemName, Class itemType) {
        Optional<LibraryItem> libraryItem = getLibraryItem(inputItemName, itemType, checkedOutLibraryItem);
        if (!libraryItem.isPresent()) {
            return false;
        }
        uncheckedOutLibraryItem.add(libraryItem.get());
        checkedOutLibraryItem.remove(libraryItem.get());
        return true;
    }

    public boolean containsItemsOf(Class itemType) {
        return !getItemList(itemType).isEmpty();
    }

    private String headerForDisplayingBookList() {
        return "List Of Available Books" + NEW_LINE +
                String.format(STRING_FORMAT_FOR_BOOK_INFORMATION, "Name", "Author", "Year Published");
    }

    private String generateStringRepresentationOf(List<LibraryItem> itemList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (LibraryItem item : itemList) {
            stringBuilder.append(item.informationRepresentationInString());
            stringBuilder.append(NEW_LINE);
        }
        return stringBuilder.toString();
    }

    private String headerForDisplayingMovieList() {
        return "List Of Available Movies" + NEW_LINE +
                String.format(STRING_FORMAT_FOR_MOVIE_INFORMATION, "Name", "Director", "Rating", "Year");
    }

    private Optional<LibraryItem> getLibraryItem(String itemName, Class itemType, List<LibraryItem> itemList) {
        return itemList.stream()
                .filter(item -> item.getClass() == itemType)
                .filter(item -> item.isSameTitle(itemName))
                .findFirst();
    }

}
