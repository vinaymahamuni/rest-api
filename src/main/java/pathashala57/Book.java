package pathashala57;

//represent a written or printed work consisting of pages
public class Book implements LibraryItem {

    private static final String STRING_FORMAT = "%-35s %-35s %-35s";
    private final String name;
    private final String author;

    public int getYearPublished() {
        return yearPublished;
    }

    private final int yearPublished;

    Book(String name, String author, int yearPublished) {
        this.name = name;
        this.author = author;
        this.yearPublished = yearPublished;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String informationRepresentationInString() {
        return String.format(STRING_FORMAT, name, author, yearPublished);
    }

    @Override
    public boolean isSameTitle(String title) {
        return this.name.equalsIgnoreCase(title);
    }

    @Override
    public boolean equals(Object that) {
        if (that == null) {
            return false;
        }
        if (this.getClass() != that.getClass()) {
            return false;
        }
        if (this == that) {
            return true;
        }
        Book anotherBook = (Book) that;
        return this.name.equals(anotherBook.name) && this.author.equals(anotherBook.author) &&
                this.yearPublished == anotherBook.yearPublished;
    }

}