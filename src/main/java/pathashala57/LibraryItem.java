package pathashala57;

public interface LibraryItem {

    String informationRepresentationInString();

    boolean isSameTitle(String title);


}
