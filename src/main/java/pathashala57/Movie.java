package pathashala57;

public class Movie implements LibraryItem {
    private static final String STRING_FORMAT = "%-35s %-35s %-35s %-35s";
    private final String name;
    private final String director;
    private final int rating;
    private final int year;
    public Movie(String name, String director, int rating, int year) {
        this.name = name;
        this.director = director;
        this.rating = rating;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public int getRating() {
        return rating;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String informationRepresentationInString() {
        return String.format(STRING_FORMAT, this.name, this.director, this.rating, this.year);
    }

    @Override
    public boolean isSameTitle(String title) {
        return this.name.equalsIgnoreCase(title);
    }

}
