package pathashala57;

import java.util.Optional;

public interface Library {

    boolean containsItemsOf(Class itemType);

    Optional<LibraryItem> checkoutItem(String itemName, Class itemType);

    String toStringRepresentationOfBooks();

    String toStringRepresentationOfMovies();

    boolean returnItem(String itemName, Class bookClass);

}
