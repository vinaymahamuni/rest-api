package pathashala57;

import java.util.ArrayList;
import java.util.List;

public class BibliotecaController {

    Biblioteca biblioteca = initializeLibrary();



    private Biblioteca initializeLibrary() {
        List<LibraryItem> libraryItems = new ArrayList<>();
        Book book = new Book("Harry Potter1", "J.K.Rowling", 2002);
        Book anotherBook = new Book("Harry Potter2", "J.K.Rowling", 2010);
        Movie movie = new Movie("Inception", "Nolan", 9, 2010);
        Movie anotherMovie = new Movie("batman", "Nolan", 10, 2001);

        libraryItems.add(book);
        libraryItems.add(anotherBook);
        libraryItems.add(movie);
        libraryItems.add(anotherMovie);
        return new Biblioteca(libraryItems);
    }

}